CFLAGS?=`pkg-config --cflags libxml-2.0` -Wall -ansi -std=c99 -D_POSIX_C_SOURCE=200809L -march=native -pipe -g -O0 -DDEBUG=1
LDFLAGS?=`pkg-config --libs libxml-2.0`
OUTOFSOURCEBUILDDIR?=/tmp/autoxrandr.w8RG0VNFz
RM?=/usr/bin/rm
RMOPTS?=-rf
MKDIR?=mkdir
MKDIROPTS?=-p
LD:=$(CC)

OBJECTS=global.o main.o configparser.o xrandr.o compute.o list.o
HEADERS=global.h config.h configparser.h xrandr.h coordinates.h defines.h compute.h list.h

$(OUTOFSOURCEBUILDDIR)/%.o: src/%.c $(addprefix src/,$(HEADERS))
	$(MKDIR) $(MKDIROPTS) $(OUTOFSOURCEBUILDDIR)
	$(CC) $(CFLAGS) -c -o $@ $<

autoxrandr: $(addprefix $(OUTOFSOURCEBUILDDIR)/,$(OBJECTS))
	$(LD) $(LDFLAGS) -o $@ $^

mrproper: clean
	$(RM) $(RMOPTS) autoxrandr

style:
	astyle --align-reference=name --align-pointer=name --indent=spaces=4 --indent-labels --pad-oper --unpad-paren --pad-header --keep-one-line-statements --convert-tabs --indent-preprocessor $(wildcard src/*.c) $(wildcard src/*.h)
	xmlindent -w $(wildcard *.xml)

clean:
	$(RM) $(RMOPTS) $(OUTOFSOURCEBUILDDIR)
	$(RM) $(RMOPTS) *~ */*~ src/*.c.orig src/*.h.orig

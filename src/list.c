/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "defines.h"
#include "list.h"

#define LIST_STRLEN ( DEFAULT_STRING_MAXLENGTH * DEFAULT_LIST_MAXCOUNT)
#define LIST_SEPARATOR '|'

char *list_create()
{
    char *result = (char *)calloc(LIST_STRLEN, sizeof(char));
    if (result != NULL) {
        result[0] = LIST_SEPARATOR;
    }
    return result;
}

void list_append(char *list, char *key)
{
    /// Silently fail on invalid arguments
    if (list == NULL || key == NULL || list[0] != LIST_SEPARATOR) return;

    int p1 = 0, p2 = 0;
    if ((p1 = strlen(list)) + (p2 = strlen(key)) < LIST_STRLEN - 2) {
        strncpy(list + p1, key, LIST_STRLEN - p1 - p2 - 2);
        list[p1 + p2] = LIST_SEPARATOR;
    }
}

int list_contains(char *list, char *key)
{
    /// Silently fail on invalid arguments
    if (list == NULL || key == NULL || list[0] != LIST_SEPARATOR) return 0;

    char buffer[DEFAULT_STRING_MAXLENGTH];
    snprintf(buffer, DEFAULT_STRING_MAXLENGTH, "|%s|", key);
    return strstr(list, buffer) != NULL;
}

int list_count(char *list)
{
    /// Silently fail on invalid argument
    if (list == NULL || list[0] != LIST_SEPARATOR) return 0;

    int result = 0;
    for (char *cur = list + 1; *cur != '\0' && cur - list < LIST_STRLEN; ++cur)
        if (*cur == LIST_SEPARATOR) ++result;
    return result;
}

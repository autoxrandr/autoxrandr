/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#include <stdio.h>

#include <X11/extensions/Xrandr.h>

#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "defines.h"
#include "global.h"
#include "config.h"
#include "configparser.h"
#include "xrandr.h"
#include "compute.h"

#define DEFAULT_STRING_MAXLENGTH  1024

void print_help() {
    fprintf(stderr, "autoXrandr -- automatically configuring X output devices\n");
    fprintf(stderr, "Copyright by Thomas Fischer <fischer@unix-ag.uni-kl.de>\n");
    fprintf(stderr, "Free software, released under the GNU GPL v3 or any later version\n\n");
    fprintf(stderr, "Usage:  autoxrandr [-f XMLCONFIGFILE] [-p PROFILE|-P] [-d DISPLAY] [-V]\n");
    fprintf(stderr, "  -f XMLCONFIGFILE   Load configuration from provided filename/path\n");
    fprintf(stderr, "  -p PROFILE         Enforce named profile instead of autodetection\n");
    fprintf(stderr, "  -P                 List known profiles and their configuration\n");
    fprintf(stderr, "  -d DISPLAY         Use this DISPLAY value instead of best guessing\n");
    fprintf(stderr, "  -V                 Verbose output\n");
}

int main(int argc, char *argv[])
{
    char displayname[DEFAULT_STRING_MAXLENGTH];
    char configurationfilename[DEFAULT_STRING_MAXLENGTH];
    int cmdlinearg_list_profiles = 0;
    displayname[0] = '\0';
    int dry_run = 0;
#ifdef DEBUG
    be_verbose = VERBOSE_HIGH;
#else // DEBUG
    be_verbose = VERBOSE_NOT;
#endif // DEBUG

    for (int i = 0; i < argc; ++i)
        /// Check if user provided '-h' or '--help' as argument
        if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
            /// Print help text and exit with exit code 1
            print_help();
            return 1;
        } else
            /// Check if user provided '-P' or '--list-profiles' as argument
            if (strcmp(argv[i], "-P") == 0 || strcmp(argv[i], "--list-profiles") == 0)
                cmdlinearg_list_profiles = 1;
            else
                /// Check if user provided '-d' or '--display' as argument
                if ((strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--display") == 0) && i < argc - 1)
                    strncpy(displayname, argv[i + 1], DEFAULT_STRING_MAXLENGTH);
                else
                    /// Check if user provided '-V' or '--verbose' as argument
                    if (strcmp(argv[i], "-V") == 0 || strcmp(argv[i], "--verbose") == 0) {
#ifndef DEBUG
                        ++be_verbose;
                        if (be_verbose > VERBOSE_HIGH) be_verbose = VERBOSE_HIGH;
#endif // DEBUG
                    }
                    else
                        /// Check if user provided '--dry-run' as argument
                        if (strcmp(argv[i], "--dry-run") == 0)
                            dry_run = 1;

    initglobal();

    if (displayname[0] == '\0')
        bestguessdisplay(displayname, DEFAULT_STRING_MAXLENGTH);
    if (displayname[0] != ':') {
        fprintf(stderr, "A valid DISPLAY value starts with \":\"\n");
        freeglobal();
        return 1;
    } else if (be_verbose >= VERBOSE_MED)
        printf("Display: %s\n", displayname);

    snprintf(configurationfilename, DEFAULT_STRING_MAXLENGTH, "%s/%s", getenv("HOME"), ".autoxrandr.xml");
    for (int i = 0; i < argc - 1; ++i)
        if (strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--configfile") == 0) {
            snprintf(configurationfilename, DEFAULT_STRING_MAXLENGTH, "%s", argv[i + 1]);
            argv[i][0] = argv[i + 1][0] = '\0'; ///< Mark arguments as processed
        }

    config *config;
    int r = config_parse(configurationfilename, &config);
    if (r != ENOERROR) {
        fprintf(stderr, "Failed to parse configuration file \"%s\"\n", configurationfilename);

        freeglobal();
        return 2;
    }

    if (cmdlinearg_list_profiles) {
        config_dump(config);

        freeglobal();
        config_parser_free();
        config_free(config);

        return 0;
    }
#ifdef DEBUG
    config_dump(config);
#endif // DEBUG

    xrandroutputlist *xol;
    r = xrandr_scan(&xol);
    if (r != ENOERROR) {
        fprintf(stderr, "Failed to run xrandr command\n");
        config_free(config);
        return 3;
    }
#ifdef DEBUG
    xrandr_dump(xol);
#endif // DEBUG

    int idx = -1;
    for (int i = 0; i < argc - 1; ++i)
        if (strcmp(argv[i], "-p") == 0 || strcmp(argv[i], "--profile") == 0) {
            for (idx = config->profilescount - 1; idx >= 0; --idx)
                if (strcmp(config->profiles[idx]->name, argv[i + 1]) == 0)
                    break;
            if (idx < 0)
                fprintf(stderr, "Profile \"%s\" not known, using auto-detection instead\n", argv[i + 1]);
            argv[i][0] = argv[i + 1][0] = '\0'; ///< Mark arguments as processed
        }
    if (idx < 0) {
        r = find_matching_condition(config, xol, &idx);
        if (r == ENOERROR) {
            if (be_verbose >= VERBOSE_LOW)
                printf("Using matching profile \"%s\" (\"%s\")\n", config->profiles[idx]->name, config->profiles[idx]->description);
        } else
            fprintf(stderr, "No matching profile found\n");
    } else {
        if (be_verbose >= VERBOSE_LOW)
            printf("Using user-requested profile \"%s\" (\"%s\")\n", config->profiles[idx]->name, config->profiles[idx]->description);
    }

    if (r == ENOERROR && idx >= 0) {
        if (be_verbose >= VERBOSE_MED)
            printf("Profile's description: %s\nProfile's priority: %d\n", config->profiles[idx]->description, config->profiles[idx]->priority);

        xrandrsettings *settings;
        r = determine_xrandr_settings(config->profiles[idx], xol, &settings);
        if (r == 0 && settings != NULL) {
#ifdef DEBUG
            xrandr_settings_dump(xol, settings);
#endif // DEBUG
            if (!dry_run)
                xrandr_settings_cmdlines(settings);
            xrandr_settings_free(settings);
        } else {
            fprintf(stderr, "Failed to determine xrandr command sequence\n");
        }
    }

    freeglobal();
    config_parser_free();

    config_free(config);
    xrandr_free(xol);

    return 0;
}

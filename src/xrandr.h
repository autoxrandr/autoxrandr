/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#ifndef XRANDR_H
#define XRANDR_H

#include "coordinates.h"
#include "configparser.h"

#define ONOFFSTATE_ON   1
#define ONOFFSTATE_OFF   2
#define ONOFFSTATE_UNCHANGED   3

typedef struct {
    char *name;
    int connected; ///< use CONNECTION_STATUS_*
    int curw, curh, curx, cury;
    int prefw, prefh;
    int supportedrescount;
    widthheight **supportedres;
} xrandroutput;

typedef struct {
    int count;
    xrandroutput **data;
} xrandroutputlist;

typedef struct {
    char *name;
    xrandroutput *xoutput;
    int isprimary;
    int onoff; ///< use ONOFFSTATE_*
    int usepreferred;
    widthheight resolution;
    int posx, posy;
    char *sameas;
    char *relativeto;
    char *relativehow;
} xrandrsetting;

typedef struct {
    int count;
    xrandrsetting **list;
} xrandrsettings;

int xrandr_scan(xrandroutputlist **detected);

#ifdef DEBUG
void xrandr_dump(xrandroutputlist *outputlist);
#endif // DEBUG

int determine_xrandr_settings(configprofile *profile, xrandroutputlist *outputlist, xrandrsettings **settings);

void xrandr_settings_cmdlines(xrandrsettings *settings);

#ifdef DEBUG
void xrandr_settings_dump(xrandroutputlist *outputlist, xrandrsettings *settings);
#endif // DEBUG

void xrandr_settings_free(xrandrsettings *settings);

void xrandr_free(xrandroutputlist *outputlist);

#endif // XRANDR_H

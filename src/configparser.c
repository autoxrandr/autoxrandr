/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#include <stdio.h>
#include <errno.h>
#include <string.h>

#include <libxml/parser.h>
#include <libxml/xpath.h>

#include "defines.h"
#include "configparser.h"

void parse_wxh(char *input, int *w, int *h) {
    *w = *h = 0;

    while (*input >= '0' && *input <= '9') {
        *w *= 10;
        *w += *input - '0';
        ++input;
    }

    if (*input != 'x') {
        *w = *h = 0;
        return;
    } else
        ++input;

    while (*input >= '0' && *input <= '9') {
        *h *= 10;
        *h += *input - '0';
        ++input;
    }

    if (*input != '\0')
        *w = *h = 0;

    return;
}

void config_sort_by_priority(config *cfg)
{
    if (cfg == NULL) return; ///< nothing to do

    /// Simple bubble sort to sort configuration profiles by priority
    /// Inspired by https://en.wikipedia.org/wiki/Bubble_sort
    int n = cfg->profilescount;
    while (n > 0) {
        int newn = 0;
        for (int i = 1; i < cfg->profilescount; ++i) {
            if (cfg->profiles[i]->priority > cfg->profiles[i - 1]->priority) {
                /// swap
                configprofile *swap = cfg->profiles[i];
                cfg->profiles[i] = cfg->profiles[i - 1];
                cfg->profiles[i - 1] = swap;
                newn = i;
            }
        }
        n = newn;
    }
}

int config_read_xpath_string_xpathObj(xmlXPathObjectPtr xpathObj, char *result, int length) {
    result[0] = '\0';
    int resultcode = EINVAL;
    if (xpathObj != NULL) {
        if (xpathObj->nodesetval != NULL && xpathObj->nodesetval->nodeNr == 1) {
            if (xpathObj->nodesetval->nodeTab[0]->children != NULL) {
                if (xpathObj->nodesetval->nodeTab[0]->children->content != NULL) {
                    strncpy(result, (char *)(xpathObj->nodesetval->nodeTab[0]->children->content), length);
                    resultcode = ENOERROR;
                } else
                    fprintf(stderr, "config_read_xpath_string_xpathObj: content is invalid\n");
            } else
                fprintf(stderr, "config_read_xpath_string_xpathObj: xpath does not have children\n");
        } else
            fprintf(stderr, "config_read_xpath_string_xpathObj: did not get exactly one node (but %d) for xpath\n", xpathObj->nodesetval->nodeNr);
    } else
        fprintf(stderr, "config_read_xpath_string_xpathObj: failed to get xpathObj\n");

    return resultcode;
}

int config_read_xpath_string_path(xmlXPathContextPtr xpathCtx, const char *path, char *result, int length) {
    xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression((const xmlChar *)path, xpathCtx);
    int resultcode = config_read_xpath_string_xpathObj(xpathObj, result, length);
    if (resultcode != ENOERROR)
        fprintf(stderr, "config_read_xpath_string_path: error occurred for path '%s'\n", path);
    xmlXPathFreeObject(xpathObj);
    return resultcode;
}

int config_read_xpath_string_context(xmlXPathContextPtr xpathCtx, xmlNodePtr contextNode, const char *path, char *result, int length) {
    xmlXPathObjectPtr xpathObj = xmlXPathNodeEval(contextNode, (const xmlChar *)path, xpathCtx);
    if (xpathObj == NULL)
        return EINVAL;
    int resultcode = config_read_xpath_string_xpathObj(xpathObj, result, length);
    if (resultcode != ENOERROR)
        fprintf(stderr, "config_read_xpath_string_context: error occurred for contextNode '%s', line %d\n", contextNode->name, contextNode->line);
    xmlXPathFreeObject(xpathObj);
    return resultcode;
}

int config_read_xpath_int_path(xmlXPathContextPtr xpathCtx, const char *path, int *result) {
    char buffer[DEFAULT_STRING_MAXLENGTH];
    buffer[0] = '\0';
    *result = 0;
    int resultcode = config_read_xpath_string_path(xpathCtx, path, buffer, DEFAULT_STRING_MAXLENGTH);
    if (resultcode == ENOERROR)
        *result = atoi(buffer);
    else
        fprintf(stderr, "config_read_xpath_int_path: error occurred for path '%s'\n", path);
    return resultcode;
}

int config_read_xpath_int_context(xmlXPathContextPtr xpathCtx, xmlNodePtr contextNode, const char *path, int *result) {
    char buffer[DEFAULT_STRING_MAXLENGTH];
    buffer[0] = '\0';
    *result = 0;
    int resultcode = config_read_xpath_string_context(xpathCtx, contextNode, path, buffer, DEFAULT_STRING_MAXLENGTH);
    if (resultcode == ENOERROR)
        *result = atoi(buffer);
    else
        fprintf(stderr, "config_read_xpath_int_context: error occurred for path '%s'\n", path);
    return resultcode;
}

int config_exists_xpath_xpathObj(xmlXPathObjectPtr xpathObj) {
    return xpathObj->nodesetval->nodeNr == 1 ? ENOERROR : EINVAL;
}

int config_exists_xpath_path(xmlXPathContextPtr xpathCtx, const char *path) {
    xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression((const xmlChar *)path, xpathCtx);
    if (xpathObj == NULL)
        return EINVAL;
    int resultcode = config_exists_xpath_xpathObj(xpathObj);
    xmlXPathFreeObject(xpathObj);
    return resultcode;
}

int config_exists_xpath_context(xmlXPathContextPtr xpathCtx, xmlNodePtr contextNode, const char *path) {
    xmlXPathObjectPtr xpathObj = xmlXPathNodeEval(contextNode, (const xmlChar *)path, xpathCtx);
    if (xpathObj == NULL)
        return EINVAL;
    int resultcode = config_exists_xpath_xpathObj(xpathObj);
    xmlXPathFreeObject(xpathObj);
    return resultcode;
}

int config_parse_requirement(xmlXPathContextPtr xpathCtx, xmlNodePtr requirementNode, configrequirement *req) {
    int resultcode = ENOERROR;

    if (strcmp((char *)(requirementNode->name), "connected") == 0) {
        req->type = REQUIREMENT_TYPE_CONNECTED;
        req->data.connected.name = (char *)calloc(DEFAULT_STRING_MAXLENGTH, sizeof(char));
        resultcode |= config_read_xpath_string_context(xpathCtx, requirementNode, "@name", req->data.connected.name, DEFAULT_STRING_MAXLENGTH);
        char buffer[DEFAULT_STRING_MAXLENGTH];
        resultcode |= config_read_xpath_string_context(xpathCtx, requirementNode, "@status", buffer, DEFAULT_STRING_MAXLENGTH);
        if (strcmp(buffer, "connected") == 0)
            req->data.connected.status = CONNECTION_STATUS_CONNECTED;
        else if (strcmp(buffer, "disconnected") == 0)
            req->data.connected.status = CONNECTION_STATUS_DISCONNECTED;
        else {
            fprintf(stderr, "config_parse_requirement: unknown connection status: %s\n", buffer);
            resultcode = EINVAL;
        }
    } else if (strcmp((char *)(requirementNode->name), "lid") == 0) {
        req->type = REQUIREMENT_TYPE_LID;

        char buffer[DEFAULT_STRING_MAXLENGTH];
        resultcode |= config_read_xpath_string_context(xpathCtx, requirementNode, "@status", buffer, DEFAULT_STRING_MAXLENGTH);
        if (strcmp(buffer, "open") == 0)
            req->data.lid.status = LIDSTATUS_OPEN;
        else if (strcmp(buffer, "closed") == 0)
            req->data.lid.status = LIDSTATUS_CLOSED;
        else
            req->data.lid.status = LIDSTATUS_UNKNOWN;
    } else {
        fprintf(stderr, "config_parse_requirement: unknown requirement node type: %s\n", requirementNode->name);
        resultcode = EINVAL;
    }

    return resultcode;
}

int config_parse_action(xmlXPathContextPtr xpathCtx, xmlNodePtr actionNode, configaction *action) {
    int resultcode = ENOERROR;

    if (strcmp((char *)(actionNode->name), "output") == 0) {
        action->type = ACTION_TYPE_OUTPUT;
        action->data.output.name = (char *)calloc(DEFAULT_STRING_MAXLENGTH, sizeof(char));
        resultcode |= config_read_xpath_string_context(xpathCtx, actionNode, "@name", action->data.output.name, DEFAULT_STRING_MAXLENGTH);

        char buffer[DEFAULT_STRING_MAXLENGTH];
        resultcode |= config_read_xpath_string_context(xpathCtx, actionNode, "@resolution", buffer, DEFAULT_STRING_MAXLENGTH);
        if (strcmp(buffer, "off") == 0) {
            action->data.output.resolution.type = RESOLUTION_TYPE_OFF;
        } else if (strcmp(buffer, "preferred") == 0 || strcmp(buffer, "auto") == 0) {
            action->data.output.resolution.type = RESOLUTION_TYPE_PREFERRED;
        } else if (strcmp(buffer, "common") == 0 || strcmp(buffer, "clone") == 0 || strcmp(buffer, "cloned") == 0) {
            action->data.output.resolution.type = RESOLUTION_TYPE_CLONE;
        } else {
            int w, h;
            parse_wxh(buffer, &w, &h);
            if (w > 0 && h > 0) {
                action->data.output.resolution.type = RESOLUTION_TYPE_PIXEL;
                action->data.output.resolution.wh.width = w;
                action->data.output.resolution.wh.height = h;
            } else {
                fprintf(stderr, "config_parse_action: unknown resolution type: %s\n", buffer);
                resultcode = EINVAL;
            }
        }

        /// Default output location is absolute at (0|0)
        action->data.output.location.type = XYPOS_TYPE_ABSOLUTE;
        action->data.output.location.data.absolute.x = action->data.output.location.data.absolute.y = 0;

        if (config_exists_xpath_context(xpathCtx, actionNode, "@relative-how") == ENOERROR) {
            buffer[0] = '\0';
            resultcode |= config_read_xpath_string_context(xpathCtx, actionNode, "@relative-how", buffer, DEFAULT_STRING_MAXLENGTH);
            if (strcmp(buffer, "left") == 0) {
                action->data.output.location.type = XYPOS_TYPE_RELATIVE;
                action->data.output.location.data.relative.relativehow = XYPOS_RELATIVEHOW_LEFT;
                action->data.output.location.data.relative.relativeto = (char *)calloc(DEFAULT_STRING_MAXLENGTH, sizeof(char));
                resultcode |= config_read_xpath_string_context(xpathCtx, actionNode, "@relative-to", action->data.output.location.data.relative.relativeto, DEFAULT_STRING_MAXLENGTH);
            }
        }
    } else {
        fprintf(stderr, "config_parse_action: unknown action node type: %s\n", actionNode->name);
        resultcode = EINVAL;
    }

    return resultcode;
}

int config_parse_profile(xmlXPathContextPtr xpathCtx, xmlNodePtr profileNode, configprofile *cp) {
    int resultcode = ENOERROR;

    resultcode |= config_read_xpath_int_context(xpathCtx, profileNode, "@priority", &(cp->priority));
    cp->name = (char *)calloc(DEFAULT_STRING_MAXLENGTH, sizeof(char));
    resultcode |= config_read_xpath_string_context(xpathCtx, profileNode, "@name", cp->name, DEFAULT_STRING_MAXLENGTH);
    cp->description = (char *)calloc(DEFAULT_STRING_MAXLENGTH, sizeof(char));
    resultcode |= config_read_xpath_string_context(xpathCtx, profileNode, "description", cp->description, DEFAULT_STRING_MAXLENGTH);

    if (resultcode == ENOERROR) { ///< still no error
        xmlXPathObjectPtr xpathObj = xmlXPathNodeEval(profileNode, (const xmlChar *)("if/*"), xpathCtx);
        if (xpathObj != NULL) {
            if (xpathObj->nodesetval != NULL) {
                cp->requirementscount = xpathObj->nodesetval->nodeNr;
                cp->requirements = (configrequirement **)calloc(cp->requirementscount, sizeof(configrequirement *));
                for (int i = 0; i < xpathObj->nodesetval->nodeNr; ++i) {
                    cp->requirements[i] = (configrequirement *)calloc(1, sizeof(configrequirement));
                    if (config_parse_requirement(xpathCtx, xpathObj->nodesetval->nodeTab[i], cp->requirements[i]) != ENOERROR) {
                        resultcode = EINVAL;
                        break;
                    }
                }
            }
            xmlXPathFreeObject(xpathObj);
        }
    }

    if (resultcode == ENOERROR) { ///< still no error
        xmlXPathObjectPtr xpathObj = xmlXPathNodeEval(profileNode, (const xmlChar *)("then/*"), xpathCtx);
        if (xpathObj != NULL) {
            if (xpathObj->nodesetval != NULL) {
                cp->actionscount = xpathObj->nodesetval->nodeNr;
                cp->actions = (configaction **)calloc(cp->actionscount, sizeof(configaction *));
                for (int i = 0; i < xpathObj->nodesetval->nodeNr; ++i) {
                    cp->actions[i] = (configaction *)calloc(1, sizeof(configaction));
                    if (config_parse_action(xpathCtx, xpathObj->nodesetval->nodeTab[i], cp->actions[i]) != ENOERROR) {
                        resultcode = EINVAL;
                        break;
                    }
                }
            }
            xmlXPathFreeObject(xpathObj);
        }
    }

    if (resultcode != ENOERROR) {
        free(cp->name);
        free(cp->description);
        for (int j = cp->requirementscount - 1; j >= 0; --j) {
            if (cp->requirements[j] != NULL && cp->requirements[j]->type == REQUIREMENT_TYPE_CONNECTED)
                free(cp->requirements[j]->data.connected.name);
            free(cp->requirements[j]);
        }
        free(cp->requirements);
        for (int j = cp->actionscount - 1; j >= 0; --j)
            free(cp->actions[j]);
        free(cp->actions);
        cp->requirementscount = cp->actionscount = 0;
        cp->name = cp->description = NULL;
        cp->requirements = NULL;
        cp->actions = NULL;
    }

    return resultcode;
}

int config_parse(char *filename, config **result)
{
    *result = NULL;
    int resultcode = ENOERROR;

    FILE *configfile = fopen(filename, "r");
    if (configfile == NULL) {
        fprintf(stderr, "Could not open file \"%s\"\n", filename);
        return errno;
    }

    char *filedata = (char *)calloc(DEFAULT_FILE_MAXSIZE, sizeof(char));
    if (filedata == NULL) {
        free(filedata);
        fclose(configfile);
        return ENOMEM;
    }

    size_t filechunklen = 0;
    size_t filedatalen = 0;
    while (ferror(configfile) == 0 && feof(configfile) == 0 && (filechunklen = fread(filedata + filedatalen, sizeof(char), DEFAULT_FILE_MAXSIZE - filedatalen, configfile)) > 0)
        filedatalen += filechunklen;
    int error = ferror(configfile);
    fclose(configfile);

    if (error != ENOERROR) {
        free(filedata);
        return error;
    } else if (filedatalen <= 0) {
        free(filedata);
        return EINVAL;
    }

    xmlDocPtr doc = xmlReadMemory(filedata, filedatalen, filename, NULL, 0);
    /// Create xpath evaluation context
    xmlXPathContextPtr xpathCtx = xmlXPathNewContext(doc);

    *result = (config *)calloc(1, sizeof(config));
    if (config_read_xpath_int_path(xpathCtx, "/autoxrandr/@version", &((*result)->version)) == ENOERROR) {
        xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression((const xmlChar *)("/autoxrandr/profile"), xpathCtx);
        if (xpathObj != NULL) {
            if ((*result)->version == 1 && xpathObj->nodesetval != NULL) {
                (*result)->profilescount = xpathObj->nodesetval->nodeNr;
                (*result)->profiles = (configprofile **)calloc((*result)->profilescount, sizeof(configprofile *));
                for (int i = 0; i < xpathObj->nodesetval->nodeNr; ++i) {
                    (*result)->profiles[i] = (configprofile *)calloc(1, sizeof(configprofile));
                    if (config_parse_profile(xpathCtx, xpathObj->nodesetval->nodeTab[i], (*result)->profiles[i]) != ENOERROR) {
                        resultcode = EINVAL;
                        break;
                    }
                }
            }
            xmlXPathFreeObject(xpathObj);
        }
    }


    xmlXPathFreeContext(xpathCtx);
    xmlFreeDoc(doc);
    free(filedata);

    if (resultcode == ENOERROR)
        config_sort_by_priority(*result);
    else {
        for (int i = (*result)->profilescount - 1; i >= 0; --i)
            free((*result)->profiles[i]);
        free((*result)->profiles);
        free(*result);
        *result = NULL;
    }

    return resultcode;
}

void config_dump(config *cfg)
{
    if (cfg == NULL) return; ///< nothing to do
    printf("# of profiles: %d\n", cfg->profilescount);

    for (int i = 0; i < cfg->profilescount; ++i) {
        configprofile *cp = cfg->profiles[i];
        printf("profile %d: \"%s\" (\"%s\"), prio %d\n", i + 1, cp->name, cp->description, cp->priority);

        printf("  # of requirements: %d\n", cp->requirementscount);
        for (int j = 0; j < cp->requirementscount; ++j) {
            printf("    requirement %d of type: ", j + 1);
            switch (cp->requirements[j]->type) {
            case REQUIREMENT_TYPE_CONNECTED:
                printf("REQUIREMENT_TYPE_CONNECTED\n");
                if (cp->requirements[j]->data.connected.name[0] == '*' && cp->requirements[j]->data.connected.name[1] == '\0')
                    printf("      any other output");
                else
                    printf("      output \"%s\"", cp->requirements[j]->data.connected.name);
                printf(" has to be ");
                switch (cp->requirements[j]->data.connected.status) {
                case CONNECTION_STATUS_CONNECTED:
                    printf("connected\n"); break;
                case CONNECTION_STATUS_DISCONNECTED:
                    printf("disconnected\n"); break;
                default:
                    printf("???\n");
                }
                break;
            case REQUIREMENT_TYPE_LID:
                printf("REQUIREMENT_TYPE_LID\n");
                printf("      lid status: ");
                switch (cp->requirements[j]->data.lid.status) {
                case LIDSTATUS_CLOSED: printf("closed\n"); break;
                case LIDSTATUS_OPEN: printf("open\n"); break;
                default: printf("unknown\n");
                }
                break;
            default: printf("unknown\n");
            }
            // TODO
        }
        printf("  # of actions: %d\n", cp->actionscount);
        for (int j = 0; j < cp->actionscount; ++j) {
            printf("    action %d of type: ", j + 1);
            switch (cp->actions[j]->type) {
            case ACTION_TYPE_OUTPUT:
                printf("ACTION_TYPE_OUTPUT\n");
                if (cp->actions[j]->data.output.name[0] == '*' && cp->actions[j]->data.output.name[1] == '\0')
                    printf("      any other output");
                else
                    printf("      output \"%s\"", cp->actions[j]->data.output.name);
                printf("\n");
                printf("      resolution is of type ");
                switch (cp->actions[j]->data.output.resolution.type) {
                case RESOLUTION_TYPE_PIXEL:
                    printf("RESOLUTION_TYPE_PIXEL (%d,%d)\n", cp->actions[j]->data.output.resolution.wh.width, cp->actions[j]->data.output.resolution.wh.height);
                    break;
                case RESOLUTION_TYPE_MAX:
                    printf("RESOLUTION_TYPE_MAX\n");
                    break;
                case RESOLUTION_TYPE_PREFERRED:
                    printf("RESOLUTION_TYPE_PREFERRED\n");
                    break;
                case RESOLUTION_TYPE_CLONE:
                    printf("RESOLUTION_TYPE_CLONE\n");
                    break;
                case RESOLUTION_TYPE_OFF:
                    printf("RESOLUTION_TYPE_OFF\n");
                    break;
                default:
                    printf("???\n");
                }

                if (cp->actions[j]->data.output.resolution.type != RESOLUTION_TYPE_OFF) {
                    printf("      ");
                    switch (cp->actions[j]->data.output.location.type) {
                    case XYPOS_TYPE_ABSOLUTE:
                        printf("at (%d,%d)\n", cp->actions[j]->data.output.location.data.absolute.x, cp->actions[j]->data.output.location.data.absolute.y);
                        break;
                    case XYPOS_TYPE_RELATIVE: {
                        char *relhow;
                        switch (cp->actions[j]->data.output.location.data.relative.relativehow) {
                        case XYPOS_RELATIVEHOW_LEFT:
                            relhow = "left";
                            break;
                        case XYPOS_RELATIVEHOW_RIGHT:
                            relhow = "right";
                            break;
                        case XYPOS_RELATIVEHOW_ABOVE:
                            relhow = "above";
                            break;
                        case XYPOS_RELATIVEHOW_BELOW:
                            relhow = "below";
                            break;
                        default:
                            relhow = "unknown";
                        }
                        printf("%s of \"%s\"\n", relhow, cp->actions[j]->data.output.location.data.relative.relativeto);
                    }
                    break;
                    default:
                        printf("at unknown position\n");
                    }
                }

                break;
            default:
                printf("unknown\n");
            }
        }
    }
}

void config_free(config *cfg)
{
    if (cfg == NULL) return; ///< nothing to do
    for (int i = cfg->profilescount - 1; i >= 0; --i) {
        configprofile *cp = cfg->profiles[i];
        free(cp->name);
        free(cp->description);
        for (int j = cp->requirementscount - 1; j >= 0; --j) {
            if (cp->requirements[j]->type == REQUIREMENT_TYPE_CONNECTED)
                free(cp->requirements[j]->data.connected.name);
            free(cp->requirements[j]);
        }
        free(cp->requirements);
        for (int j = cp->actionscount - 1; j >= 0; --j) {
            if (cp->actions[j]->type == ACTION_TYPE_OUTPUT) {
                free(cp->actions[j]->data.output.name);
                if (cp->actions[j]->data.output.location.type == XYPOS_TYPE_RELATIVE)
                    free(cp->actions[j]->data.output.location.data.relative.relativeto);
            }
            free(cp->actions[j]);
        }
        free(cp->actions);
        free(cp);
    }
    free(cfg->profiles);
    free(cfg);
}


void config_parser_free()
{
    /// Cleanup function for the XML library.
    xmlCleanupParser();
}

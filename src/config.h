/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

#define REQUIREMENT_TYPE_CONNECTED    1001
#define REQUIREMENT_TYPE_LID          1002
#define ACTION_TYPE_OUTPUT            2001
#define RESOLUTION_TYPE_PIXEL         2101
#define RESOLUTION_TYPE_MAX           2102
#define RESOLUTION_TYPE_PREFERRED     2103
#define RESOLUTION_TYPE_CLONE         2104
#define RESOLUTION_TYPE_OFF           2105
#define LIDSTATUS_OPEN                3001
#define LIDSTATUS_CLOSED              3002
#define LIDSTATUS_UNKNOWN             3003

#include "coordinates.h"

/**
 * @brief Description of an output's resolution, either in pixel or some 'relative' value.
 */
typedef struct {
    int type; ///< type of resolution, see definitions starting with 'RESOLUTION_TYPE_'
    widthheight wh; ///< if @p type is RESOLUTION_TYPE_PIXEL, this is the resolution in pixel
} resolution;

/**
 * @brief Description of requirements/preconditions for a configuration profile.
 */
typedef struct {
    int type; ///< type of requirement, see definitions starting with 'REQUIREMENT_TYPE_'
    union { ///< which structure to use depends on @p type
        struct { ///< REQUIREMENT_TYPE_CONNECTED which tests whether an output is connected
            char *name; ///< name of the output as used by 'xrandr'
            int status; ///< connection status of an output, defined by 'CONNECTION_STATUS_' defines
        } connected;
        struct { ///< REQUIREMENT_TYPE_LID which tests whether the lid is open or closed
            int status; ///< lid status, defined by 'LIDSTATUS_' defines
        } lid;
    } data;
} configrequirement;

/**
 * @brief Description of actions to take once a profile gets activated.
 */
typedef struct {
    int type; ///< type of action, see definitions starting with 'ACTION_TYPE_'
    union { ///< which structure to use depends on @p type
        struct { ///< ACTION_TYPE_OUTPUT which (de)activates and configures an output
            char *name; ///< name of the output as used by 'xrandr'
            resolution resolution; ///< preferred resolution for this output
            position location; ///< location of this output in relation to other outputs
        } output;
    } data;
} configaction;

/**
 * @brief Individual configuration profile like 'presentation mode' or 'desktop'.
 */
typedef struct {
    int priority; ///< numeric value describing the preference for this profile
    char *name; ///< name of this profile
    char *description; ///< human-readable description of this profile
    configrequirement **requirements; ///< technical description of conditions triggering this profile
    int requirementscount; ///< number of conditions in @p requirements
    configaction **actions; ///< actions to take once this profile gets activated
    int actionscount; ///< number of actions on @p actions
} configprofile;

/**
 * @brief Main data structure to store a configuration file.
 */
typedef struct {
    int version; ///< version of the configuration data structure. Currently only version '1' is supported.
    configprofile **profiles; ///< list of configuration profiles
    int profilescount; ///< number of profiles in @p profiles
} config;

#endif // CONFIG_H

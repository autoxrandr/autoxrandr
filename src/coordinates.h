/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2015 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#ifndef COORDINATES_H
#define COORDINATES_H

#define XYPOS_TYPE_ABSOLUTE        5001
#define XYPOS_TYPE_RELATIVE        5002
#define XYPOS_RELATIVEHOW_LEFT     5010
#define XYPOS_RELATIVEHOW_RIGHT    5011
#define XYPOS_RELATIVEHOW_ABOVE    5012
#define XYPOS_RELATIVEHOW_BELOW    5013

typedef struct {
    int width, height;
} widthheight;

typedef struct {
    int type; ///< See XYPOS_TYPE_*
    union { ///< which structure to use depends on @p type
        struct { ///< XYPOS_TYPE_ABSOLUTE
            int x, y;
        } absolute;
        struct { ///< XYPOS_TYPE_RELATIVE
            char *relativeto; ///< name of an output
            int relativehow; ///< XYPOS_RELATIVEHOW_*
        } relative;
    } data;
} position;

#endif // COORDINATES_H


/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#ifndef GLOBAL_H
#define GLOBAL_H

#define VERBOSE_NOT    0
#define VERBOSE_LOW    1
#define VERBOSE_MED    2
#define VERBOSE_HIGH   3

int be_verbose;

int initglobal();
int freeglobal();

int envdisplay(char *buffer, int len);
int userdisplay(char *username, char *buffer, int len);
int bestguessdisplay(char *buffer, int len);

#endif // GLOBAL_H

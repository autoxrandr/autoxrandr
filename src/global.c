/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <utmp.h>
#include <unistd.h>

#include "defines.h"
#include "global.h"

char **screennames;
int screennamescount;

int initglobal()
{
    screennames = (char **)calloc(DEFAULT_LIST_MAXCOUNT, sizeof(char *));
    screennamescount = 0;

    /// Open directory /tmp/.X11-unix to read sockets for each running display
    DIR *tmpX11unix = opendir("/tmp/.X11-unix");
    if (tmpX11unix == NULL) {
        /// could not open or access /tmp/.X11-unix
        fprintf(stderr, "Could not open/access \"/tmp/.X11-unix\" (error code %d: %s)\n", errno, strerror(errno));
        return errno;
    }

    /// Read all entries /tmp/.X11-unix, extract screen names (like "0")
    /// and store list in "screennames"
    struct dirent *fileentry;
    while ((fileentry = readdir(tmpX11unix)) != NULL && errno == 0 && screennamescount < DEFAULT_LIST_MAXCOUNT) {
        /// Skip over directory entries not starting with 'X'
        if (fileentry->d_name[0] != 'X') continue;

        screennames[screennamescount] = strndup(fileentry->d_name + 1, DEFAULT_LIST_MAXCOUNT);
        ++screennamescount;
    }

    /// Clean up: Release handles
    closedir(tmpX11unix);

    return ENOERROR;
}

int freeglobal()
{
    /// Clean up: Free memory
    for (int i = screennamescount - 1; i >= 0; --i)
        free(screennames[i]);
    free(screennames);

    return ENOERROR;
}

int envdisplay(char *buffer, int len)
{
    /// Clear string in case no DISPLAY is defined
    buffer[0] = '\0';

    /// Copy
    strncpy(buffer, getenv("DISPLAY"), len);

    /// Documentation of strncpy says:
    /// If there is no null byte among the first n bytes of src,
    /// the string placed in dest will not be null-terminated.
    buffer[len - 1] = '\0';

    return ENOERROR;
}

int userdisplay(char *username, char *buffer, int len)
{
    /// Initialize buffer to be empty
    buffer[0] = '\0';
    buffer[len - 1] = '\0';

/// The ut_host field in struct utmp is required
/// There is no point in doing anything if that is not available
#ifdef _HAVE_UT_HOST

    /// No point in doing anything if not at least one screen was found
    if (screennamescount > 0) {
        /// Get ready to check all logged-in users
        setutent();

        /// Test each accounted-for user, stop as soon as match has been found
        /// Test of accounted user's name equals "username" and user is using X11
        struct utmp *user;
        while ((user = getutent()) != NULL && buffer[0] == '\0') {
            if (strncmp(user->ut_user, username, DEFAULT_STRING_MAXLENGTH) == 0)
                for (int i = screennamescount - 1; i >= 0 && buffer[0] == '\0'; --i) {
                    /// For X11-using users, hostname is like ":0.0"
                    /// So, check for starting ':', then compare to screennames like "0:0"
                    if (user->ut_host[0] == ':' && strncmp(screennames[i], user->ut_host + 1, DEFAULT_STRING_MAXLENGTH) == 0)
                        strncpy(buffer, user->ut_host, len - 1);
                }
        }

        /// Close reading user accounting database
        endutent();
    }

#endif // _HAVE_UT_HOST

    return ENOERROR;
}

int bestguessdisplay(char *buffer, int len)
{
    /// Check environment variable DISPLAY
    int r = envdisplay(buffer, len);
    if (r == 0 && buffer[0] != '\0')
        return ENOERROR;

    /// Determine current user
    char *username = getlogin();
    if (username != NULL) {
        /// Check display for this user
        r = userdisplay(username, buffer, len);
        if (r == 0 && buffer[0] != '\0')
            return ENOERROR;
    }

    /// Pick first display known to system
    if (screennamescount > 0) {
        buffer[0] = ':';
        strncpy(buffer + 1, screennames[0], len - 1);
        return ENOERROR;
    }

    /// No screens found, clear buffer
    buffer[0] = '\0';

    return EINVAL;
}

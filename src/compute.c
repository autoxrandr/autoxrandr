/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "defines.h"
#include "list.h"
#include "compute.h"

/**
 * @brief Build a list of output names where the
 * corresponding outputs have the requested status.
 *
 * @param outputlist xrandr output list to evaluate
 * @param connection_status connection status to filter for (defines starting with 'CONNECTION_STATUS_')
 * @param list_string the functions result, a list of outputs (empty list if no outputs found)
 */
void list_of_outputs_by_connstatus(xrandroutputlist *outputlist, int connection_status, char **list_string)
{
    *list_string = list_create();
    for (int i = 0; i < outputlist->count; ++i) {
        if (outputlist->data[i]->connected != connection_status)
            continue;
        list_append(*list_string, outputlist->data[i]->name);
    }
}

int find_common_resolution(xrandroutputlist *outputlist, widthheight *common_res)
{
    if (common_res == NULL || outputlist == NULL) {
        /// Nowhere to write result to or no output list to process
        return EINVAL;
    }

    if (outputlist->count == 0) {
        /// No common resolution found, set return value to 0x0
        common_res->width = 0;
        common_res->height = 0;
        return EINVAL;
    }

    xrandroutput *firstoutput = outputlist->data[0];
    common_res->width = firstoutput->prefw;
    common_res->height = firstoutput->prefh;
    int all_share_same_prefresolution = 1;
    for (int i = 1; all_share_same_prefresolution > 0 && i < outputlist->count; ++i)
        all_share_same_prefresolution = outputlist->data[i]->prefw == common_res->width && outputlist->data[i]->prefh == common_res->height;
    if (all_share_same_prefresolution)
        return ENOERROR; ///< Best result: all outputs share same preferred resolution

    /// Assumption: each output's valid resolutions are sorted from large to small
    for (int i = 0; i < firstoutput->supportedrescount; ++i) {
        /// Go through all of the first output's resolutions,
        /// check if they exist at every other output
        common_res->width = firstoutput->supportedres[i]->width;
        common_res->height = firstoutput->supportedres[i]->height;

        int resolution_exists_everywhere = 1;
        for (int j = 1; resolution_exists_everywhere > 0 && j < outputlist->count; ++j) {
            int supported_here = 0;
            for (int k = 0; supported_here == 0 && k < outputlist->data[j]->supportedrescount; ++k)
                supported_here = outputlist->data[j]->supportedres[k]->width == common_res->width && outputlist->data[j]->supportedres[k]->height == common_res->height;
            resolution_exists_everywhere = supported_here;
        }

        if (resolution_exists_everywhere)
            return ENOERROR; ///< Second-best result: all outputs share first output's i-th resolution
    }

    /// No common resolution found, set return value to 0x0
    common_res->width = 0;
    common_res->height = 0;
    return EINVAL;
}

int find_matching_condition(config *cfg, xrandroutputlist *outputlist, int *config_index_res)
{
    *config_index_res = -1;
    if (cfg == NULL || cfg->profilescount < 1 || outputlist == NULL || outputlist->count < 1) {
        /// either configuration or xrandr output list is invalid
        return EINVAL;
    }

    char *listofconnectedoutputs, *listofdisconnectedoutputs;
    list_of_outputs_by_connstatus(outputlist, CONNECTION_STATUS_CONNECTED, &listofconnectedoutputs);
    list_of_outputs_by_connstatus(outputlist, CONNECTION_STATUS_DISCONNECTED, &listofdisconnectedoutputs);

    for (int idx = 0; idx < cfg->profilescount; ++idx) {
        /// Initally assume that this profile matches,
        /// variable becomes 0 as soon as contrdicting information is found
        int is_match = 1;

        /// Keep track of which outputs have been checked/processed
        char *processedoutputs = list_create();

        /// First pass for exact rules
        for (int j = 0; j < cfg->profiles[idx]->requirementscount; ++j) {
            configrequirement *cr = cfg->profiles[idx]->requirements[j];
            if (cr->type == REQUIREMENT_TYPE_CONNECTED) {
                if (cr->data.connected.name[0] == '*') {
                    /// process output "*" later in second pass
                    continue;
                }

                if (cr->data.connected.status == CONNECTION_STATUS_CONNECTED && !list_contains(listofconnectedoutputs, cr->data.connected.name)) {
                    /// output is not connected, although it was required to be connected
                    is_match = 0;
                    break;
                } else if (cr->data.connected.status == CONNECTION_STATUS_DISCONNECTED && !list_contains(listofdisconnectedoutputs, cr->data.connected.name)) {
                    /// output is connected, although it was required to be disconnected
                    is_match = 0;
                    break;
                }

                /// Don't handle just this output in second pass
                list_append(processedoutputs, cr->data.connected.name);
            } else if (cr->type == REQUIREMENT_TYPE_LID) {
                FILE *procfs = fopen("/proc/acpi/button/lid/LID/state", "r");
                if (procfs != NULL) {
                    char buffer[DEFAULT_STRING_MAXLENGTH];
                    if (fgets(buffer, DEFAULT_STRING_MAXLENGTH, procfs) != NULL) {
                        fclose(procfs);
                        int status = strstr(buffer, "open") != NULL ? LIDSTATUS_OPEN : (strstr(buffer, "closed") != NULL ? LIDSTATUS_CLOSED : LIDSTATUS_UNKNOWN);
                        is_match = status == cr->data.lid.status ? 1 : 0;
                        if (is_match == 0) break;
                    } else {
                        fclose(procfs);
                        is_match = 0;
                        break;
                    }
                } else {
                    is_match = 0;
                    break;
                }
            } else {
                /// Any other REQUIREMENT_TYPE_* not (yet) supported
                is_match = 0;
                break;
            }
        }

        /// Second pass for fuzzy matches, e.g. '*'
        for (int j = 0; is_match == 1 && j < cfg->profiles[idx]->requirementscount; ++j) {
            configrequirement *cr = cfg->profiles[idx]->requirements[j];
            if (cr->type == REQUIREMENT_TYPE_CONNECTED ///< Only REQUIREMENT_TYPE_CONNECTED supported
                    && cr->data.connected.name[0] == '*') {
                /// Process fuzzy match on any non-processed output
                for (int k = 0; k < outputlist->count; ++k) {
                    if (!list_contains(processedoutputs, outputlist->data[k]->name) && outputlist->data[k]->connected != cr->data.connected.status) {
                        /// Fuzzy match required all other outputs to be (dis)connected,
                        /// but just this one failed (i.e. is (dis)connected contrary to expectations)
                        is_match = 0;
                        break;
                    }
                }
            }
        }

        free(processedoutputs);

        if (is_match) {
            *config_index_res = idx;
            free(listofconnectedoutputs);
            free(listofdisconnectedoutputs);
            return ENOERROR;
        }
    }

    free(listofconnectedoutputs);
    free(listofdisconnectedoutputs);
    return EINVAL;
}

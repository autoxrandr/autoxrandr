/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#ifndef DEFINES_H
#define DEFINES_H

#ifndef ENOERROR
#define ENOERROR    0
#endif // ENOERROR

#define DEFAULT_FILE_MAXSIZE    65536
#define DEFAULT_LIST_MAXCOUNT   256
#define DEFAULT_STRING_MAXLENGTH  1024
#define DEFAULT_RESOLUTION_MAXPIXEL   8192

#define CONNECTION_STATUS_CONNECTED   1
#define CONNECTION_STATUS_DISCONNECTED   -1

#endif // DEFINES_H

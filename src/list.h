/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#ifndef LIST_H
#define LIST_H

/**
 * This 'module' provides a very simple list for char* strings.
 * This implementation has the following limitations:
 * - The number of items is limited by the fixed size allocated
 *   for each list (LIST_STRLEN, by default 256k minus overhead).
 * - The items added to the lists may not contain the character
 *   LIST_SEPARATOR (by default '|').
 * - Run-time complexity is disastrous, but acceptable for small,
 *   limited lists.
 */

/**
 * Create and allocate space for an empty, fixed-size list.
 * The list is represented in a char* string and can be passed
 * as an argument to other functions of this module.
 * If allocating space fails, this function will return NULL.
 */
char *list_create();

/**
 * Append a string 'key' to a list. The key may not contain the
 * character LIST_SEPARATOR. If adding the key to the list
 * would exceed the list's capacity, adding the key will silently
 * fail. If either argument is invalid (NULL or not a list),
 * this function will silently fail.
 * In this function's context, 'failing' include not adding the
 * key.
 */
void list_append(char *list, char *key);

/**
 * Checks if a given key is contained in the list. If zero is
 * returned, the key is not contained, if non-zero is returned
 * it is contained. If either argument is invalid (NULL or not
 * a list), this function will silently fail returning zero.
 */
int list_contains(char *list, char *key);

/**
 * Counts the number of key (items) in the list. This function
 * returns a non-negative result. If the argument is invalid
 * (NULL or not a list), this function will silently fail
 * returning zero.
 */
int list_count(char *list);

#endif // LIST_H

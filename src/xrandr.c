/****************************************************************************
 *
 *  This file is part of autoXrandr.
 *  Copyright 2012-2014 by Thomas Fischer <fischer@unix-ag.uni-kl.de>
 *
 *  autoXrandr is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  autoXrandr is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with autoXrandr.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <sys/wait.h>

#include "defines.h"
#include "compute.h"
#include "list.h"
#include "global.h"
#include "xrandr.h"

char *xrandrbinary = "/usr/bin/xrandr";

int xrandr_scan(xrandroutputlist **detected)
{
    *detected = NULL;

    int pipefd[2];

    /// use pipe to read xrandr's output into autoxrandr
    int r = pipe(pipefd);
    if (r != 0) ///< some error in pipe(..)
        return errno;

    /// fork to run xrandr in child process
    pid_t pid = fork();
    if (pid == 0) {
        /// inside child process

        close(pipefd[0]); ///< close pipe's reading end

        dup2(pipefd[1], 1); ///< stdout to pipe
        dup2(pipefd[1], 2); ///< stderr to pipe

        close(pipefd[1]); ///< close pipe's writing end

        /// run "xrandr -q"
        char *argv[3];
        argv[0] = xrandrbinary;
        argv[1] = "-q";
        argv[2] = NULL;
        if (execvp(argv[0], argv) != 0)
            _exit(errno);
    } else if (pid == -1) {
        /// error condition
        return errno;
    } else {
        /// inside parent process

        close(pipefd[1]); ///< close pipe's writing end

        /// treat pipe like a file for line-based processing
        FILE *execoutput = fdopen(pipefd[0], "r");
        char line[DEFAULT_STRING_MAXLENGTH];

        int numlines = 0;
        /// go through all files ...
        char *rc = fgets(line, DEFAULT_STRING_MAXLENGTH, execoutput);
        while (rc != NULL && !feof(execoutput)) {
            if (line[0] == '\0')
                continue;
            ++numlines;

            /// find outputs like LVDS, VGA1, HDMI1, ...
            char *outputname = NULL;
            char *t1 = NULL, *t2 = NULL;
            if ((t1 = strstr(line, " connected ")) != NULL)
                outputname = strndup(line, t1 - line); ///< example: "VGA connected 1024x768+0+0"
            else if ((t2 = strstr(line, " disconnected ")) != NULL)
                outputname = strndup(line, t2 - line); ///< example: "VGA disconnected"

            if (outputname != NULL) {
                if (*detected == NULL) {
                    *detected = (xrandroutputlist *)calloc(1, sizeof(xrandroutputlist));
                    (*detected)->count = 0;
                    (*detected)->data = (xrandroutput **)calloc(DEFAULT_LIST_MAXCOUNT, sizeof(xrandroutput *));
                }

                xrandroutput *xo = (xrandroutput *)calloc(1, sizeof(xrandroutput));
                (*detected)->data[(*detected)->count++] = xo;
                xo->name = outputname;
                xo->connected = t1 != NULL ? CONNECTION_STATUS_CONNECTED : CONNECTION_STATUS_DISCONNECTED;

                /// read in the output's current size and position
                if (t1 != NULL) t2 = t1;
                t1 = strstr(t2, "x"); ///< locate the 'x' in "VGA connected 1024x768+0+0"
                xo->cury = xo->curx = INT_MIN;
                if (*(t1 - 1) >= '0' && *(t1 - 1) <= '9' && *(t1 + 1) >= '0' && *(t1 + 1) <= '9') {
                    t2 = t1;
                    while (*(t2 - 1) >= '0' && *(t2 - 1) <= '9') --t2;
                    char *next1 = NULL, *next2 = NULL;
                    long int l = strtol(t2, &next1, 10);
                    xo->curw = (int)l;
                    if (next1[0] == 'x') {
                        long int l = strtol(next1 + 1, &next2, 10);
                        xo->curh = (int)l;
                        if (next2[0] == '+') {
                            long int l = strtol(next2 + 1, &next1, 10);
                            xo->curx = (int)l;
                            if (next1[0] == '+') {
                                long int l = strtol(next1 + 1, &next2, 10);
                                xo->cury = (int)l;
                            }
                        }
                    }
                }

                /// initialize data structure for all supported resolutions
                xo->supportedrescount = 0;
                xo->supportedres = (widthheight **)calloc(DEFAULT_LIST_MAXCOUNT, sizeof(widthheight *));

                rc = fgets(line, DEFAULT_STRING_MAXLENGTH, execoutput);
                while (rc != NULL && !feof(execoutput) && line[0] == ' ' && line[1] == ' ' && line[2] == ' ' && line[3] >= '1' && line[3] <= '9') {
                    char *next1 = NULL, *next2 = NULL;
                    long int resx = strtol(line + 3, &next1, 10);
                    if (resx < 0 || resx > DEFAULT_RESOLUTION_MAXPIXEL) break;
                    if (next1[0] != 'x') break;
                    long int resy = strtol(next1 + 1, &next2, 10);
                    if (resy < 0 || resy > DEFAULT_RESOLUTION_MAXPIXEL) break;

                    widthheight *wh = (widthheight *)calloc(1, sizeof(widthheight));
                    xo->supportedres[xo->supportedrescount++] = wh;
                    wh->width = (int)resx;
                    wh->height = (int)resy;

                    if (xo->prefw <= 0 && xo->prefw <= 0 && strstr(next2, "+") != NULL) {
                        /// is preferred resolution
                        xo->prefw = wh->width;
                        xo->prefh = wh->height;
                    } else if (xo->curw <= 0 && xo->curh <= 0 && strstr(next2, "*") != NULL) {
                        /// is current resolution
                        xo->curw = wh->width;
                        xo->curh = wh->height;
                    }

                    rc = fgets(line, DEFAULT_STRING_MAXLENGTH, execoutput);
                }
            } else {
                rc = fgets(line, DEFAULT_STRING_MAXLENGTH, execoutput);
            }
        }

        fclose(execoutput);
        close(pipefd[0]); ///< close pipe's reading end

        if (numlines <= 0) {
            return EINVAL;
        }
    }

    return ENOERROR;
}

#ifdef DEBUG
void xrandr_dump(xrandroutputlist *outputlist)
{
    if (outputlist == NULL)
        return;

    printf("# of outputs: %d\n", outputlist->count);
    for (int i = 0; i < outputlist->count; ++i) {
        printf("  output %d: \"%s\"\n", i + 1, outputlist->data[i]->name);
        if (outputlist->data[i]->connected == CONNECTION_STATUS_CONNECTED) {
            printf("    connected, ");
            if (outputlist->data[i]->curw > 0 && outputlist->data[i]->curh > 0)
                printf("size %dx%d at pos %d|%d\n", outputlist->data[i]->curw, outputlist->data[i]->curh, outputlist->data[i]->curx, outputlist->data[i]->cury);
            else
                printf("but disabled\n");
            printf("    # of supported resolutions: %d\n", outputlist->data[i]->supportedrescount);
            for (int j = 0; j < outputlist->data[i]->supportedrescount; ++j) {
                printf("      resolution %d: %dx%d", j + 1, outputlist->data[i]->supportedres[j]->width, outputlist->data[i]->supportedres[j]->height);
                if (outputlist->data[i]->supportedres[j]->width == outputlist->data[i]->curw && outputlist->data[i]->supportedres[j]->height == outputlist->data[i]->curh)
                    printf(", current");
                if (outputlist->data[i]->supportedres[j]->width == outputlist->data[i]->prefw && outputlist->data[i]->supportedres[j]->height == outputlist->data[i]->prefh)
                    printf(", preferred");
                printf("\n");
            }
        } else if (outputlist->data[i]->connected == CONNECTION_STATUS_DISCONNECTED)
            printf("    disconnected\n");
        else
            printf("    unknown\n");
    }
}
#endif // DEBUG

int determine_xrandr_settings(configprofile *profile, xrandroutputlist *outputlist, xrandrsettings **settings)
{
    *settings = NULL;

    if (profile == NULL || profile->actionscount < 1 || outputlist == NULL || outputlist->count < 1) {
        /// invalid input data
        return EINVAL;
    }

    *settings = (xrandrsettings *)calloc(1, sizeof(xrandrsettings));
    (*settings)->count = 0;
    (*settings)->list = (xrandrsetting **)calloc(DEFAULT_LIST_MAXCOUNT, sizeof(xrandrsetting *));

    /// Keep track of which outputs have been checked/processed
    char *processedoutputs = list_create();
    char *sameoutput = list_create();

    for (int i = profile->actionscount - 1; i >= 0; --i) {
        configaction *action = profile->actions[i];
        if (action->type != ACTION_TYPE_OUTPUT) {
            /// Only output changes considered here
            continue;
        }
        if (action->data.output.name[0] == '*' && action->data.output.name[1] == '\0') {
            /// Output name is asterisk ('*')
            continue;
        }

        xrandrsetting *os = (xrandrsetting *)calloc(1, sizeof(xrandrsetting));
        (*settings)->list[(*settings)->count++] = os;

        os->name = strdup(action->data.output.name);
        os->xoutput = NULL;
        for (int j = 0; j < outputlist->count; ++j)
            if (strcmp(outputlist->data[j]->name, os->name) == 0) {
                os->xoutput = outputlist->data[j];
                break;
            }
        os->onoff = action->data.output.resolution.type == RESOLUTION_TYPE_OFF ? ONOFFSTATE_OFF : ONOFFSTATE_ON;
        os->usepreferred = action->data.output.resolution.type == RESOLUTION_TYPE_PREFERRED;

        if (action->data.output.resolution.type == RESOLUTION_TYPE_CLONE) {
            list_append(sameoutput, action->data.output.name);
        } else if (action->data.output.resolution.type == RESOLUTION_TYPE_PIXEL) {
            os->resolution.width = action->data.output.resolution.wh.width;
            os->resolution.height = action->data.output.resolution.wh.height;
        }

        os->posx = os->posy = INT_MIN;
        if (action->data.output.location.type == XYPOS_TYPE_RELATIVE) {
            os->relativeto = strdup(action->data.output.location.data.relative.relativeto);
            switch (action->data.output.location.data.relative.relativehow) {
            case XYPOS_RELATIVEHOW_LEFT:
                os->relativehow = "left-of";
                break;
            case XYPOS_RELATIVEHOW_RIGHT:
                os->relativehow = "right-of";
                break;
            case XYPOS_RELATIVEHOW_ABOVE:
                os->relativehow = "above";
                break;
            case XYPOS_RELATIVEHOW_BELOW:
                os->relativehow = "below";
                break;
            }
        } else if (action->data.output.location.type == XYPOS_TYPE_ABSOLUTE) {
            os->posx = action->data.output.location.data.absolute.x;
            os->posx = action->data.output.location.data.absolute.y;
        }

        list_append(processedoutputs, action->data.output.name);
    }

    for (int i = profile->actionscount - 1; i >= 0; --i) {
        configaction *action = profile->actions[i];
        if (action->type != ACTION_TYPE_OUTPUT) {
            /// Only output changes considered here
            continue;
        }
        if (action->data.output.name[0] != '*' || action->data.output.name[1] != '\0') {
            /// Omit all output actions except those addressing '*'
            continue;
        }

        for (int j = 0; j < outputlist->count; ++j) {
            char *outputname = outputlist->data[j]->name;
            if (!list_contains(processedoutputs, outputname)) {
                xrandrsetting *os = (xrandrsetting *)calloc(1, sizeof(xrandrsetting));
                (*settings)->list[(*settings)->count++] = os;

                os->name = strdup(outputname);
                os->xoutput = NULL;
                for (int j = 0; j < outputlist->count; ++j)
                    if (strcmp(outputlist->data[j]->name, os->name) == 0) {
                        os->xoutput = outputlist->data[j];
                        break;
                    }
                os->onoff = ONOFFSTATE_OFF; // FIXME
                os->posx = os->posy = INT_MIN;

                // TODO

                list_append(processedoutputs, outputname);
            }
        }

        break;
    }

    /// All unnamed outputs shall stay unchanged
    for (int i = 0; i < outputlist->count; ++i) {
        char *outputname = outputlist->data[i]->name;
        if (!list_contains(processedoutputs, outputname)) {
            xrandrsetting *os = (xrandrsetting *)calloc(1, sizeof(xrandrsetting));
            (*settings)->list[(*settings)->count++] = os;
            os->name = strdup(outputname);
            os->onoff = ONOFFSTATE_UNCHANGED;
        }
    }

    widthheight whforclones;

    /// Determine if multiple outputs were selected to be cloned
    int sameoutput_count = list_count(sameoutput);
    if (sameoutput_count > 1) {
        /// Assemble a list of all xrandr outputs that are to be cloned
        xrandroutputlist *clonedoutputlist = (xrandroutputlist *)calloc(1, sizeof(xrandroutputlist));
        clonedoutputlist->count = sameoutput_count;
        clonedoutputlist->data = (xrandroutput **)calloc(outputlist->count, sizeof(xrandroutput *));
        for (int i = 0, idx = 0; i < outputlist->count; ++i)
            if (list_contains(sameoutput, outputlist->data[i]->name))
                clonedoutputlist->data[idx++] = outputlist->data[i];
        /// Determine common resolution for those outputs
        int success_for_common_resolution = find_common_resolution(clonedoutputlist, &whforclones);
        free(clonedoutputlist->data);
        free(clonedoutputlist);

        if (success_for_common_resolution == ENOERROR)
            printf("Found common resolution for cloned outputs: %dx%d\n", whforclones.width, whforclones.height);
        else
            printf("Did not find common resolution for cloned outputs, using each output's preferred resolution\n");

        char *firstclonename;
        int firstclone = 1;
        for (int i = 0; i < (*settings)->count; ++i)
            if (list_contains(sameoutput, (*settings)->list[i]->name)) {
                /// Fall-back if "clones" don't share a common resolution:
                /// everyone uses preferred resolution
                (*settings)->list[i]->usepreferred = success_for_common_resolution != ENOERROR;
                (*settings)->list[i]->resolution.width = success_for_common_resolution != ENOERROR ? 0 : whforclones.width;
                (*settings)->list[i]->resolution.height = success_for_common_resolution != ENOERROR ? 0 : whforclones.height;
                if (firstclone) {
                    firstclone = 0;
                    firstclonename = (*settings)->list[i]->name;
                } else
                    (*settings)->list[i]->sameas = strdup(firstclonename);
            }
    } else {
        /// No clones a.k.a. same-as outputs
    }

    free(sameoutput);
    free(processedoutputs);

    return ENOERROR;
}


void xrandr_settings_cmdline(xrandrsetting *setting) {
    char buffer[DEFAULT_STRING_MAXLENGTH];
    char **args = (char **)calloc(DEFAULT_LIST_MAXCOUNT, sizeof(char *));
    int nextarg = 0;

    args[nextarg] = strdup(xrandrbinary);
    ++nextarg;

    args[nextarg] = strdup("--display");
    ++nextarg;

    envdisplay(buffer, DEFAULT_STRING_MAXLENGTH);
    args[nextarg] = strdup(buffer);
    ++nextarg;

    args[nextarg] = strdup("--output");
    ++nextarg;

    args[nextarg] = strdup(setting->name);
    ++nextarg;

    if (setting->onoff == ONOFFSTATE_OFF)
        args[nextarg] = strdup("--off");
    else
        args[nextarg] = strdup("--auto");
    ++nextarg;

    if (setting->onoff != ONOFFSTATE_OFF) {
        if (setting->usepreferred) {
            args[nextarg] = strdup("--preferred");
            ++nextarg;
        } else if (setting->resolution.width > 0 && setting->resolution.height > 0) {
            args[nextarg] = strdup("--mode");
            ++nextarg;
            args[nextarg] = (char *)calloc(DEFAULT_STRING_MAXLENGTH, sizeof(char));
            snprintf(args[nextarg], DEFAULT_STRING_MAXLENGTH, "%dx%d", setting->resolution.width, setting->resolution.height);
            ++nextarg;
        }
        if (setting->posx > INT_MIN && setting->posy > INT_MIN) {
            args[nextarg] = strdup("--pos");
            ++nextarg;
            args[nextarg] = (char *)calloc(DEFAULT_STRING_MAXLENGTH, sizeof(char));
            snprintf(args[nextarg], DEFAULT_STRING_MAXLENGTH, "%dx%d",  setting->posx, setting->posy);
            ++nextarg;
        }

        if (setting->sameas != NULL) {
            args[nextarg] = strdup("--same-as");
            ++nextarg;
            args[nextarg] = strdup(setting->sameas);
            ++nextarg;
        }
        if (setting->relativehow != NULL && setting->relativeto != NULL) {
            args[nextarg] = (char *)calloc(DEFAULT_STRING_MAXLENGTH, sizeof(char));
            snprintf(args[nextarg], DEFAULT_STRING_MAXLENGTH, "--%s",  setting->relativehow);
            ++nextarg;
            args[nextarg] = strdup(setting->relativeto);
            ++nextarg;
        }
    }

    if (be_verbose >= VERBOSE_MED) {
        for (int j = 0; j < nextarg && args[j] != NULL; ++j) {
            if (j != 0) printf(" ");
            printf("%s", args[j]);
        }
        printf("\n");
    }

    int pid = -1;
    if ((pid = fork()) == -1) {
        fprintf(stderr, "Failed to fork\n");
        exit(1);
    } else if (pid == 0) {
        /// Inside child

        execvp(args[0], args);

        fprintf(stderr, "Failed to run command: %s\n", args[0]);
        exit(1);
    } else {
        /// Inside parent, child's PID is in variable 'pid'
        int p = -1;
        int status = 0;
        if ((p = waitpid(pid, &status, 0)) != pid) {
            int e = errno;
            fprintf(stderr, "Something went wrong while waiting for process %d (returned pid=%d, errno=%d)\n", pid, p, e);
        } else if (WIFEXITED(status)) {
            if (WEXITSTATUS(status) != 0)
                printf("Child process %i exited with status %d\n", pid, WEXITSTATUS(status));
        } else if (WIFSIGNALED(status)) {
            printf("Child process %i exited due to signal %d\n", pid, WTERMSIG(status));
        } else {
            printf("Child process %i exited due to an unknown reason\n", pid);
        }
    }

    for (int j = 0; j < nextarg; ++j)
        if (args[j] != NULL)
            free(args[j]);
        else
            break;
    free(args);
}

void xrandr_settings_cmdlines(xrandrsettings *settings) {
    if (settings == NULL) return; ///< nothing to do

    for (int i = 0; i < settings->count; ++i) {
        if (settings->list[i]->onoff != ONOFFSTATE_OFF) ///< Process only "off" screens
            continue;

        xrandr_settings_cmdline(settings->list[i]);
    }

    for (int i = 0; i < settings->count; ++i) {
        if (settings->list[i]->onoff == ONOFFSTATE_OFF) ///< "off" screens had been processed above
            continue;
        if (settings->list[i]->relativehow != NULL || settings->list[i]->relativeto != NULL || settings->list[i]->sameas != NULL) ///< skip screens that are relative to others or clones
            continue;

        xrandr_settings_cmdline(settings->list[i]);
    }

    for (int i = 0; i < settings->count; ++i) {
        if (settings->list[i]->onoff == ONOFFSTATE_OFF) ///< "off" screens had been processed above
            continue;
        if (settings->list[i]->relativehow == NULL || settings->list[i]->relativeto == NULL || settings->list[i]->sameas == NULL) ///< non-relative or non-cloned screens had been processed above
            continue;

        xrandr_settings_cmdline(settings->list[i]);
    }

}

#ifdef DEBUG
void xrandr_settings_dump(xrandroutputlist *xol, xrandrsettings *settings)
{
    if (settings == NULL) return; ///< nothing to do

    printf("# of settings to make: %d\n", settings->count);
    for (int i = 0; i < settings->count; ++i) {
        printf("  setting %d on output \"%s\"\n", i + 1, settings->list[i]->name);
        switch (settings->list[i]->onoff) {
        case ONOFFSTATE_ON: printf("    switching on\n"); break;
        case ONOFFSTATE_OFF: printf("    switching off\n"); break;
        case ONOFFSTATE_UNCHANGED: printf("    keeping on/off state\n"); break;
        }
        if (settings->list[i]->usepreferred) {
            int prefw = 0, prefh = 0;
            for (int i = xol->count - 1; i >= 0; --i)
                if (strcmp(xol->data[i]->name, settings->list[i]->name) == 0) {
                    prefw = xol->data[i]->prefw;
                    prefh = xol->data[i]->prefh;
                }
            printf("    using preferred resolution");
            if (prefw > 0 && prefh > 0)
                printf(" (%d,%d)", prefw, prefh);
            printf("\n");
        } else if (settings->list[i]->resolution.width > 0 && settings->list[i]->resolution.height > 0)
            printf("    using resolution (%d,%d)\n", settings->list[i]->resolution.width, settings->list[i]->resolution.height);
        if (settings->list[i]->sameas != NULL)
            printf("    same as \"%s\"\n", settings->list[i]->sameas);
        if (settings->list[i]->posx > INT_MIN && settings->list[i]->posy > INT_MIN)
            printf("    at (%d,%d)\n", settings->list[i]->posx, settings->list[i]->posy);
        else if (settings->list[i]->relativehow != NULL && settings->list[i]->relativeto != NULL)
            printf("    %s of \"%s\"\n", settings->list[i]->relativehow, settings->list[i]->relativeto);
    }
}
#endif // DEBUG

void xrandr_settings_free(xrandrsettings *settings)
{
    if (settings == NULL) return; ///< nothing to do

    for (int i = settings->count - 1; i >= 0; --i) {
        if (settings->list[i]->name != NULL)
            free(settings->list[i]->name);
        if (settings->list[i]->sameas != NULL)
            free(settings->list[i]->sameas);
        free(settings->list[i]);
    }

    free(settings->list);
    free(settings);
}

void xrandr_free(xrandroutputlist *outputlist)
{
    if (outputlist == NULL)
        return;
    for (int i = outputlist->count - 1; i >= 0; --i) {
        free(outputlist->data[i]->name);
        for (int j = outputlist->data[i]->supportedrescount - 1; j >= 0; --j)
            free(outputlist->data[i]->supportedres[j]);
        free(outputlist->data[i]->supportedres);
        free(outputlist->data[i]);
    }
    free(outputlist->data);
    free(outputlist);
}
